$(document).on('submit', "#login-user", function(e){
	e.preventDefault();
	let datos = {
		"username" : $("#usuario-email").val(),
		"userpass" : $("#usuario-clave").val() 
	}
	$.ajax({
		url:"/signin",
		method:"POST",
		dataType:"JSON",
		data: {
			"data":btoa(JSON.stringify(datos))
		},
		beforeSend:function(){
			$("button[type=submit]").attr('disabled', 'true');
		}
	}).then(function(response){
		
	}).catch(function(request){

	})
});

/*Verficacion de datos en el form*/
$(document).on('change', '#usuario-email',function(e){
	let texto = $("#usuario-email").val();
	if(texto.match(/\w*.\w*\@sapi.gob.ve/) == null){
		$("#usuario-email").addClass('is-invalid');
		$("button[type=submit]").attr('disabled', 'true');
	}
	else if(texto.lenght < 5){
		$("#usuario-email").addClass('is-invalid');
		$("button[type=submit]").attr('disabled', 'true');
	}
	else{
		$("#usuario-email").removeClass('is-invalid');
		$("#usuario-email").addClass('is-valid');
		$("button[type=submit]").removeAttr('disabled');	
	}
});