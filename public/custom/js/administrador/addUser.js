//Funcion que carga los roles

function loadRoles(){
  $.ajax({
    url:"/getRoles",
    method:"POST",
    dataType:"JSON"
  }).then((response) => {

  }).catch((request) =>{
    
  });
}


//Verificacion si las dos contraseñas son iguales
$(document).on('keyup', "#user-confirm-pass", (e) => {
  e.preventDefault();
  let pass = $("#user-confirm-pass").val();
  if($("#user-pass").val() != pass){
    $("#user-pass").addClass('is-invalid');
    $("#user-confirm-pass").addClass('is-invalid');
    $("button[type=submit]").attr('disabled', 'true');
  }
  else{
    $("#user-pass").removeClass('is-invalid');
    $("#user-confirm-pass").removeClass('is-invalid');
    $("#user-pass").addClass('is-valid');
    $("#user-confirm-pass").addClass('is-valid');
    $("button[type=submit]").removeAttr('disabled');
  }
});

//Guardado de los datos
$(document).on('submit', "#new-user", function(e){
  e.preventDefault();
  let datos = {
    "username": $("#user-name").val(),
    "userlastname": $("#user-lastname").val(),
    "useremail":$("#user-email").val(),
    "userrol":$("#user-rol").val(),
    "userpass":$("#user-pass").val()
  }
})