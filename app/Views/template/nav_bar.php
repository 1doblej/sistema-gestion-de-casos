<?php $session = session();?>
<body class="hold-transition layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-dark navbar-primary layout-navbar-fixed">
    <div class="container">
      <a href="/inicio" class="navbar-brand">
        <img src="<?php echo base_url();?>/theme/img/Logosapi-2020.png" alt="Sapi Logo" class="brand-image"
             style="opacity: .8">
      </a>
      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="/inicio" class="nav-link">Inicio</a>
          </li>
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Casos</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <li><a href="/nuevoCaso/<?php echo $session->get('userid');?>" class="dropdown-item">Nuevo Caso</a></li>
              <li><a href="/historicoCasos/<?php echo $session->get('userid');?>" class="dropdown-item">Historico de casos</a></li>
              <li><a href="/historicoSeguimientos/<?php echo $session->get('userid');?>" class="dropdown-item">Seguimientos</a></li>
            </ul>
          </li>
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Reportes</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <li><a href="/reporteusuario/<?php echo $session->get('userid')?>/1" class="dropdown-item">Casos generados</a></li>
              <li><a href="/reporteusuario/<?php echo $session->get('userid')?>/2" class="dropdown-item">Casos por fecha</a></li>
              <li><a href="/reporteusuario/<?php echo $session->get('userid')?>/3" class="dropdown-item">Casos cerrados</a></li>
              <li><a href="/reporteusuario/<?php echo $session->get('userid')?>/4" class="dropdown-item">Casos con seguimiento</a></li>
              <li><a href="/reporteusuario/<?php echo $session->get('userid')?>/5" class="dropdown-item">Casos sin seguimiento</a></li>
              <li class="dropdown-divider"></li>
              <li><a href="/consolidadoUsuario/<?php echo $session->get('userid')?>" class="dropdown-item">Consolidados</a></li>
            </ul>
          </li>
          <?php if($session->get('userprofile') == 1){?>
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Administrador</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <li><a href="/adminUsers" class="dropdown-item">Gestión de Usuarios</a></li>
              <li><a href="/adminRoles" class="dropdown-item">Añadir y quitar Roles</a></li>
            </ul>
          </li>
        <?php }?>
        <?php if($session->get('userprofile') == 2){?>
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Reportes</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <li><a href="/reportesupervisor/1" class="dropdown-item">Por Estatus</a></li>
              <li><a href="/reportesupervisor/2" class="dropdown-item">Por Fecha</a></li>
              <li><a href="/reportesupervisor/3" class="dropdown-item">Por Usuario</a></li>
              <li class="dropdown-divider"></li>
              <li><a href="/consolidadoSupervisor" class="dropdown-item">Consolidados</a></li>
            </ul>
          </li>
        <?php }?>
        </ul>
      </div>

      <!-- Right navbar links -->
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
            <span class="ml-2 d-none d-lg-block">
              <span class="text-default"><i class="fa fa-user"></i><?php echo $session->get('username');?></span>
            </span>
          </a>
          <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
            <a class="dropdown-item" href="/perfil/<?php echo $session->get('userid')?>">
              <i class="dropdown-icon fe fe-user"></i> Perfil
            </a>
            <a class="dropdown-item" href="#">
              <i class="dropdown-icon fe fe-log-out"></i> Cerrar Sesión
            </a>
          </div>
        </li>
      </ul>
    </div>
  </nav>
  <!-- /.navbar -->