<!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Creado por Bryan Useche
    </div>
    <!-- Default to the left -->
    <strong>Copyleft &copy; 2020 <a href="http://sapi.gob.ve">Servicio Autónomo de la Propiedad Intelectual</a>.</strong>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="<?php echo base_url();?>/theme/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url();?>/theme/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>/theme/dist/js/adminlte.min.js"></script>
</body>
</html>