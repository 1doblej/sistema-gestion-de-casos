<?php 
namespace App\Models;

class Sgc_usuarios extends BaseModel{

	//Metodo para obtener el usuario solo por el correo electronico

	public function obtenerUsuario(String $correo){
		$builder = $this->dbconn('sgc_usuario_operador a');
		$builder->select('*');
		$builder->join('sgc_roles b', 'b.idrol = a.idrol');
		$builder->where('a.usuopemail', $correo);
		$query = $builder->get();
		return $query;
	}

	//Metodo para obtener todos los usuarios

	public function getAllUsers(){
		$builder = $this->dbconn('sgc_usuario_operador a');
		$builder->join("sgc_roles b",'a.idrol = b.idrol');
		$query = $builder->get();
		return $query;
	}
}
