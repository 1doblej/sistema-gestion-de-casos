<?php 
namespace App\Models;

use CodeIgniter\Model;

class BaseModel extends Model{
	public function dbconn($Tabla){
		$db      = \Config\Database::connect();
		$builder = $db->table($Tabla);
		return $builder;
	}
} 