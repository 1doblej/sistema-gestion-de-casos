<?php
namespace App\Controllers;

use App\Models\Sgc_usuarios;
use CodeIgniter\API\ResponseTrait;

class Login extends BaseController{
	use ResponseTrait;

	public function auth(){
		$model = new Sgc_usuarios();
		$session = session();
		$userdata = array();
		if($this->request->isAJAX() && !$this->session->get('logged')){
			$datos = json_decode(base64_decode($this->request->getPost('data')));
			$query = $model->autUser($datos["username"]);
			if($query->resultID->num_rows > 0){
				foreach ($query->getResult() as $row) {
					if(password_verify($datos["password"], $row->usuopemail)){
						$userdata["nombre"] = ucwords($row->usuopnom).' '.ucwords($row->usuopape);
						$userdata["userrol"] = $row->idrol;
						$userdata["iduser"] = $row->idusuopr;
						$session->set($userdata);
						return $this->respond(["message" => "Iniciando sesion"],200);
					}
					else{
						return $this->respond(["message" => "Clave incorrecta"],401);
					}
				}
			}
			else{
				return $this->respond(["message" => "Usuario o contraseña incorrectos"],404);
			}
		}
		else if($this->session->get('logged')){
			return redirect()->to('/inicio');
		}
		else{
			return redirect()->to('/403');
		}
	}

}