<?php namespace App\Controllers;

use App\Models\Sgc_usuarios;

class Administrador extends BaseController{

	//Metodo que carga la vista de los usuarios registrados en el sistema
	public function adminUsers(){
		$model = new Sgc_usuarios();
		$headings = array('ID', "Nombre", "Apellido","Rol de Usuario","Correo Electronico","Acciones");
		$rows = array();
		if($this->session->get('userprofile') == 1 || is_null($this->session->get('userprofile'))){
			$query = $model->getAllUsers();
			if($query->resultID->num_rows > 0){
				foreach($query->getResult() as $row){
					$rows[] = array($row->idusuop, $row->usuopnom, $row->usuopape,$row->rolnom,$row->usuopemail);
				}
			}
			else{
				$rows[] = array('<td colspan="6">Sin Registros</td>','','','','',"");
			}
			$data["tabla"] = $this->generarTabla($headings,$rows);
			return $this->loadTemplate('administrador/usuarios',$data);
		}
		else{
			return redirect()->to('/403');
		}
	}

	//Metodo que obtiene los roles del sistema
	public function obtenerRoles(){
		
	}

	
}