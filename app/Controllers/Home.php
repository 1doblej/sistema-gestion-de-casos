<?php namespace App\Controllers;

use App\Models\Sgc_usuarios;

class Home extends BaseController
{
	public function index()
	{
		return view('welcome_message');
	}

	//--------------------------------------------------------------------

	//Todas las vistas deben ser cargadas aqui
	public function login(){
		echo view('template/header');
		echo view('login/content');
		echo view('login/footer');
	}

	//Vista principal

	public function dashboard(){
		return $this->loadTemplate('dashboard');
	}

	//Vista de carga de un nuevo caso
	public function nuevoCaso(){
		return $this->loadTemplate('casos/nuevo');
	}
}
