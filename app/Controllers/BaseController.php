<?php
namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;

class BaseController extends Controller
{

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = [];

	/**
	 * Constructor.
	 */
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.:
		$this->session = \Config\Services::session();
	}

	//Metodo para cargar la plantilla
	public function loadTemplate($modulo, Array $data = NULL){
		echo view('template/header');
		if($modulo != 'login'){
			echo view('template/nav_bar');
			if($data != NULL){
				echo view($modulo.'/content', $data);
			}
			else{
				echo view($modulo.'/content');
			}
			echo view('template/footer');
			echo view($modulo.'/footer');
		}
		else{
			echo view('login/content');
			echo view('template/footer');
			echo view('login/footer');
		}
	}

	/*Funcion que formatea fechas*/
    public function formatearFecha($fecha){
        $date1 = explode('-',$fecha);
        $date2 = $date1[2]."-".$date1[1]."-".$date1[0];
        return $date2;
    }
     /**
	 * Metodo que permite generar tablas debidamente formateadas
	 * usando Bootstrap4
	 * 
	 *
	 * @var heading : Array  => Arreglo con las cabeceras del usuario
	 * @var data    : Array   => Los datos de la tabla puestos en un arreglo de arreglos
	 * 
	 */
    public function generarTabla(Array $heading, Array $data){
    	$this->table = new \CodeIgniter\View\Table();

    	$template = [
	        'table_open'        => '<table class="table table-light table-striped text-center tabla">',

	        'thead_open'        => '<thead>',
	        'thead_close'       => '</thead>',

	        'heading_row_start' => '<tr>',
	        'heading_row_end'   => '</tr>',
	        'heading_cell_start'=> '<th>',
	        'heading_cell_end'      => '</th>',

	        'tfoot_open'             => '<tfoot>',
	        'tfoot_close'            => '</tfoot>',

	        'footing_row_start'      => '<tr>',
	        'footing_row_end'        => '</tr>',
	        'footing_cell_start'     => '<td>',
	        'footing_cell_end'       => '</td>',

	        'tbody_open'            => '<tbody>',
	        'tbody_close'           => '</tbody>',

	        'row_start'             => '<tr>',
	        'row_end'               => '</tr>',
	        'cell_start'            => '<td>',
	        'cell_end'              => '</td>',

	        'row_alt_start'         => '<tr>',
	        'row_alt_end'           => '</tr>',
	        'cell_alt_start'        => '<td>',
	        'cell_alt_end'          => '</td>',

	        'table_close'           => '</table>'
	    ];
	    $this->table->setTemplate($template);
	    $this->table->setHeading($heading);
	    return $this->table->generate($data);
    }
}
