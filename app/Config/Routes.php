<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::login');
$routes->get('/inicio', "Home::dashboard");
$routes->get('/perfil/(:num)','Perfil::perfil/$1');
//Rutas para la generacion de casos por los Usuarios
$routes->get('/nuevoCaso',"Caso::index");
$routes->get('/historicoCasos/(:num)',"Historico::index/$1");
$routes->get('/historicoSeguimientos/(:num)',"Seguimiento::index/$1");
//Rutas para los reportes de los usuarios
$routes->get('/reporteusuario/(:num)/(:num)',"Reporte::porUsuario/$1/$2");
$routes->get('/consolidadoUsuario/(:num)', "Reporte::consolidadoUsuario/$1");
//Rutas para el administrador del sistema
$routes->get('/adminUsers',"Administrador::adminUsers");
$routes->get('/adminRoles',"Administrador::adminRoles");
$routes->post('/getRoles', "Administrador::obtenerRoles");
//Rutas para el supervisor
$routes->get('/reportesupervisor/(:num)',"Reporte::porSupervisor/$1");
$routes->get('/consolidadoSupervisor',"Reporte::consolidadoSupervisor");
//Rutas generales de la aplicacion
$routes->get('/403', "Home::forbidden");
$routes->get('/404', "Home::notFound");



/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
