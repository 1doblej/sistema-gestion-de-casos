
CREATE SEQUENCE sgc_parroquias_parroquiaid_seq_1;

CREATE TABLE sgc_parroquias (
                parroquiaid INTEGER NOT NULL DEFAULT nextval('sgc_parroquias_parroquiaid_seq_1'),
                parroquianom VARCHAR(48) NOT NULL,
                CONSTRAINT parroquiaid PRIMARY KEY (parroquiaid)
);
COMMENT ON TABLE sgc_parroquias IS 'Tabla donde estaran las parroquias de los estados';


ALTER SEQUENCE sgc_parroquias_parroquiaid_seq_1 OWNED BY sgc_parroquias.parroquiaid;

CREATE SEQUENCE sgc_municipio_municipioid_seq_1;

CREATE TABLE sgc_municipio (
                municipioid INTEGER NOT NULL DEFAULT nextval('sgc_municipio_municipioid_seq_1'),
                municipionom VARCHAR(48) NOT NULL,
                CONSTRAINT municipioid PRIMARY KEY (municipioid)
);
COMMENT ON TABLE sgc_municipio IS 'Tabla donde estaran asociados los municipios';


ALTER SEQUENCE sgc_municipio_municipioid_seq_1 OWNED BY sgc_municipio.municipioid;

CREATE SEQUENCE munpar_munparid_seq;

CREATE TABLE munpar (
                munparid INTEGER NOT NULL DEFAULT nextval('munpar_munparid_seq'),
                parroquiaid INTEGER NOT NULL,
                municipioid INTEGER NOT NULL,
                CONSTRAINT munpar PRIMARY KEY (munparid)
);
COMMENT ON TABLE munpar IS 'Tabla de union entre municipios y parroquias';


ALTER SEQUENCE munpar_munparid_seq OWNED BY munpar.munparid;

CREATE SEQUENCE sgc_estados_estadoid_seq_1;

CREATE TABLE sgc_estados (
                estadoid INTEGER NOT NULL DEFAULT nextval('sgc_estados_estadoid_seq_1'),
                estadonom VARCHAR(48) NOT NULL,
                CONSTRAINT estado_id PRIMARY KEY (estadoid)
);
COMMENT ON TABLE sgc_estados IS 'Tabla maestra donde estaran los estados de Venezuela';


ALTER SEQUENCE sgc_estados_estadoid_seq_1 OWNED BY sgc_estados.estadoid;

CREATE SEQUENCE sgc_estado_municipio_estmunid_seq;

CREATE TABLE sgc_estado_municipio (
                estmunid INTEGER NOT NULL DEFAULT nextval('sgc_estado_municipio_estmunid_seq'),
                estadoid INTEGER NOT NULL,
                municipioid INTEGER NOT NULL,
                CONSTRAINT id PRIMARY KEY (estmunid)
);
COMMENT ON TABLE sgc_estado_municipio IS 'Tabla de union entre estados y municipios';


ALTER SEQUENCE sgc_estado_municipio_estmunid_seq OWNED BY sgc_estado_municipio.estmunid;

CREATE SEQUENCE sgc_roles_idrol_seq_1;

CREATE TABLE sgc_roles (
                idrol INTEGER NOT NULL DEFAULT nextval('sgc_roles_idrol_seq_1'),
                rolnom VARCHAR(48) NOT NULL,
                CONSTRAINT idrol PRIMARY KEY (idrol)
);
COMMENT ON TABLE sgc_roles IS 'Tabla de roles del sistema';


ALTER SEQUENCE sgc_roles_idrol_seq_1 OWNED BY sgc_roles.idrol;

CREATE SEQUENCE sgc_usuario_operador_idusuopr_seq_1;

CREATE TABLE sgc_usuario_operador (
                idusuopr INTEGER NOT NULL DEFAULT nextval('sgc_usuario_operador_idusuopr_seq_1'),
                usuopnom VARCHAR(48) NOT NULL,
                usuopape VARCHAR(48) NOT NULL,
                usuoppass VARCHAR(128) NOT NULL,
                idrol INTEGER NOT NULL,
                usuopemail VARCHAR(48) NOT NULL,
                CONSTRAINT idusuopr PRIMARY KEY (idusuopr)
);
COMMENT ON TABLE sgc_usuario_operador IS 'Tabla de almacenaje de los usuarios del sistema';


ALTER SEQUENCE sgc_usuario_operador_idusuopr_seq_1 OWNED BY sgc_usuario_operador.idusuopr;

CREATE SEQUENCE sgc_estatus_llamadas_idestllam_seq;

CREATE TABLE sgc_estatus_llamadas (
                idestllam INTEGER NOT NULL DEFAULT nextval('sgc_estatus_llamadas_idestllam_seq'),
                estllamnom VARCHAR(48) NOT NULL,
                CONSTRAINT idestllam PRIMARY KEY (idestllam)
);
COMMENT ON TABLE sgc_estatus_llamadas IS 'Tabla de los estatus de las llamadas';


ALTER SEQUENCE sgc_estatus_llamadas_idestllam_seq OWNED BY sgc_estatus_llamadas.idestllam;

CREATE SEQUENCE sgc_estatus_idest_seq;

CREATE TABLE sgc_estatus (
                idest INTEGER NOT NULL DEFAULT nextval('sgc_estatus_idest_seq'),
                estnom VARCHAR(48) NOT NULL,
                CONSTRAINT idest PRIMARY KEY (idest)
);
COMMENT ON TABLE sgc_estatus IS 'Tabla maestra de los estatus de los tramites';


ALTER SEQUENCE sgc_estatus_idest_seq OWNED BY sgc_estatus.idest;

CREATE SEQUENCE sgc_direccion_correspondiente_iddircor_seq;

CREATE TABLE sgc_direccion_correspondiente (
                iddircor INTEGER NOT NULL DEFAULT nextval('sgc_direccion_correspondiente_iddircor_seq'),
                dircornom VARCHAR(48) NOT NULL,
                CONSTRAINT iddircorr PRIMARY KEY (iddircor)
);
COMMENT ON TABLE sgc_direccion_correspondiente IS 'Tabla de las direcciones al cual remiten los casos';


ALTER SEQUENCE sgc_direccion_correspondiente_iddircor_seq OWNED BY sgc_direccion_correspondiente.iddircor;

CREATE SEQUENCE sgc_tipo_prop_intelec_idtippropint_seq;

CREATE TABLE sgc_tipo_prop_intelec (
                idtippropint INTEGER NOT NULL DEFAULT nextval('sgc_tipo_prop_intelec_idtippropint_seq'),
                tippropnom VARCHAR(48) NOT NULL,
                CONSTRAINT idtippropint PRIMARY KEY (idtippropint)
);
COMMENT ON TABLE sgc_tipo_prop_intelec IS 'Tabla de los tipos de propiedad intelectual';


ALTER SEQUENCE sgc_tipo_prop_intelec_idtippropint_seq OWNED BY sgc_tipo_prop_intelec.idtippropint;

CREATE SEQUENCE sgc_tipo_req_usu_idtiprequsu_seq;

CREATE TABLE sgc_tipo_req_usu (
                idtiprequsu INTEGER NOT NULL DEFAULT nextval('sgc_tipo_req_usu_idtiprequsu_seq'),
                tiprequsunom VARCHAR(48) NOT NULL,
                CONSTRAINT idrequsu PRIMARY KEY (idtiprequsu)
);
COMMENT ON TABLE sgc_tipo_req_usu IS 'Tabla de los tipos de requerimientos que tiene el usuario';


ALTER SEQUENCE sgc_tipo_req_usu_idtiprequsu_seq OWNED BY sgc_tipo_req_usu.idtiprequsu;

CREATE SEQUENCE sgc_red_social_idrrss_seq;

CREATE TABLE sgc_red_social (
                idrrss INTEGER NOT NULL DEFAULT nextval('sgc_red_social_idrrss_seq'),
                rsnom VARCHAR(48) NOT NULL,
                CONSTRAINT rrssid PRIMARY KEY (idrrss)
);
COMMENT ON TABLE sgc_red_social IS 'Tabla de almacenamiento de las redes sociales a utilizar';


ALTER SEQUENCE sgc_red_social_idrrss_seq OWNED BY sgc_red_social.idrrss;

CREATE SEQUENCE sgc_casos_idcaso_seq;

CREATE TABLE sgc_casos (
                idcaso INTEGER NOT NULL DEFAULT nextval('sgc_casos_idcaso_seq'),
                casofec DATE NOT NULL,
                casoced VARCHAR(48) NOT NULL,
                casonom VARCHAR(48) NOT NULL,
                casoape VARCHAR(48) NOT NULL,
                casotel VARCHAR(48) NOT NULL,
                casonumsol VARCHAR(48),
                casoavz VARCHAR(48) NOT NULL,
                idest INTEGER NOT NULL,
                idrrss INTEGER NOT NULL,
                idusuopr INTEGER NOT NULL,
                estadoid INTEGER NOT NULL,
                municipioid INTEGER NOT NULL,
                parroquiaid INTEGER NOT NULL,
                CONSTRAINT idcaso PRIMARY KEY (idcaso)
);
COMMENT ON TABLE sgc_casos IS 'Tabla de almacenaje de los casos';
COMMENT ON COLUMN sgc_casos.casonumsol IS 'Numero de tramite o de solicitud';
COMMENT ON COLUMN sgc_casos.casoavz IS 'Avance del caso';
COMMENT ON COLUMN sgc_casos.idusuopr IS 'Usuario responsable';


ALTER SEQUENCE sgc_casos_idcaso_seq OWNED BY sgc_casos.idcaso;

CREATE SEQUENCE sgc_seguimiento_caso_idsegcas_seq;

CREATE TABLE sgc_seguimiento_caso (
                idsegcas INTEGER NOT NULL DEFAULT nextval('sgc_seguimiento_caso_idsegcas_seq'),
                idcaso INTEGER NOT NULL,
                idestllam INTEGER NOT NULL,
                segcoment VARCHAR(512) NOT NULL,
                segfec TIMESTAMP NOT NULL,
                CONSTRAINT idseg PRIMARY KEY (idsegcas)
);
COMMENT ON TABLE sgc_seguimiento_caso IS 'Tabla de almacenaje de los seguimentos de los casos';


ALTER SEQUENCE sgc_seguimiento_caso_idsegcas_seq OWNED BY sgc_seguimiento_caso.idsegcas;

CREATE SEQUENCE sgc_dir_cor_caso_iddircorcaso_seq;

CREATE TABLE sgc_dir_cor_caso (
                iddircorcaso INTEGER NOT NULL DEFAULT nextval('sgc_dir_cor_caso_iddircorcaso_seq'),
                iddircor INTEGER NOT NULL,
                idcaso INTEGER NOT NULL,
                CONSTRAINT iddircorcaso PRIMARY KEY (iddircorcaso)
);
COMMENT ON TABLE sgc_dir_cor_caso IS 'Tabla de union entre los casos y las direcciones';


ALTER SEQUENCE sgc_dir_cor_caso_iddircorcaso_seq OWNED BY sgc_dir_cor_caso.iddircorcaso;

CREATE SEQUENCE sgc_tipo_req_usu_caso_idtiporeqcaso_seq;

CREATE TABLE sgc_tipo_req_usu_caso (
                idtiporeqcaso INTEGER NOT NULL DEFAULT nextval('sgc_tipo_req_usu_caso_idtiporeqcaso_seq'),
                idtiprequsu INTEGER NOT NULL,
                idcaso INTEGER NOT NULL,
                CONSTRAINT idtiporeqcaso PRIMARY KEY (idtiporeqcaso)
);
COMMENT ON TABLE sgc_tipo_req_usu_caso IS 'Tabla de union entre los requerimientos del usuario y los casos';


ALTER SEQUENCE sgc_tipo_req_usu_caso_idtiporeqcaso_seq OWNED BY sgc_tipo_req_usu_caso.idtiporeqcaso;

CREATE SEQUENCE sgc_tipo_prop_caso_idtipopropcaso_seq;

CREATE TABLE sgc_tipo_prop_caso (
                idtipopropcaso INTEGER NOT NULL DEFAULT nextval('sgc_tipo_prop_caso_idtipopropcaso_seq'),
                idtippropint INTEGER NOT NULL,
                idcaso INTEGER NOT NULL,
                CONSTRAINT idtipoprop PRIMARY KEY (idtipopropcaso)
);
COMMENT ON TABLE sgc_tipo_prop_caso IS 'Tabla de union entre los casos y los tipos de propiedad intelectual';


ALTER SEQUENCE sgc_tipo_prop_caso_idtipopropcaso_seq OWNED BY sgc_tipo_prop_caso.idtipopropcaso;

ALTER TABLE munpar ADD CONSTRAINT sgc_parroquias_munpar_fk
FOREIGN KEY (parroquiaid)
REFERENCES sgc_parroquias (parroquiaid)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE sgc_casos ADD CONSTRAINT sgc_parroquias_sgc_casos_fk
FOREIGN KEY (parroquiaid)
REFERENCES sgc_parroquias (parroquiaid)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE sgc_estado_municipio ADD CONSTRAINT sgc_municipio_sgc_estado_municipio_fk
FOREIGN KEY (municipioid)
REFERENCES sgc_municipio (municipioid)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE munpar ADD CONSTRAINT sgc_municipio_munpar_fk
FOREIGN KEY (municipioid)
REFERENCES sgc_municipio (municipioid)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE sgc_casos ADD CONSTRAINT sgc_municipio_sgc_casos_fk
FOREIGN KEY (municipioid)
REFERENCES sgc_municipio (municipioid)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE sgc_estado_municipio ADD CONSTRAINT sgc_estados_sgc_estado_municipio_fk
FOREIGN KEY (estadoid)
REFERENCES sgc_estados (estadoid)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE sgc_casos ADD CONSTRAINT sgc_estados_sgc_casos_fk
FOREIGN KEY (estadoid)
REFERENCES sgc_estados (estadoid)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE sgc_usuario_operador ADD CONSTRAINT sgc_roles_sgc_usuario_operador_fk
FOREIGN KEY (idrol)
REFERENCES sgc_roles (idrol)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE sgc_casos ADD CONSTRAINT sgc_usuario_operador_sgc_casos_fk
FOREIGN KEY (idusuopr)
REFERENCES sgc_usuario_operador (idusuopr)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE sgc_seguimiento_caso ADD CONSTRAINT sgc_estatus_llamadas_sgc_seguimiento_caso_fk
FOREIGN KEY (idestllam)
REFERENCES sgc_estatus_llamadas (idestllam)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE sgc_casos ADD CONSTRAINT sgc_estatus_sgc_casos_fk
FOREIGN KEY (idest)
REFERENCES sgc_estatus (idest)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE sgc_dir_cor_caso ADD CONSTRAINT sgc_direccion_correspondiente_sgc_dir_cor_caso_fk
FOREIGN KEY (iddircor)
REFERENCES sgc_direccion_correspondiente (iddircor)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE sgc_tipo_prop_caso ADD CONSTRAINT sgc_tipo_prop_intelec_sgc_tipo_prop_caso_fk
FOREIGN KEY (idtippropint)
REFERENCES sgc_tipo_prop_intelec (idtippropint)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE sgc_tipo_req_usu_caso ADD CONSTRAINT sgc_tipo_req_usu_sgc_tipo_req_usu_caso_fk
FOREIGN KEY (idtiprequsu)
REFERENCES sgc_tipo_req_usu (idtiprequsu)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE sgc_casos ADD CONSTRAINT sgc_red_social_sgc_casos_fk
FOREIGN KEY (idrrss)
REFERENCES sgc_red_social (idrrss)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE sgc_tipo_prop_caso ADD CONSTRAINT sgc_casos_sgc_tipo_prop_caso_fk
FOREIGN KEY (idcaso)
REFERENCES sgc_casos (idcaso)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE sgc_tipo_req_usu_caso ADD CONSTRAINT sgc_casos_sgc_tipo_req_usu_caso_fk
FOREIGN KEY (idcaso)
REFERENCES sgc_casos (idcaso)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE sgc_dir_cor_caso ADD CONSTRAINT sgc_casos_sgc_dir_cor_caso_fk
FOREIGN KEY (idcaso)
REFERENCES sgc_casos (idcaso)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE sgc_seguimiento_caso ADD CONSTRAINT sgc_casos_sgc_seguimiento_caso_fk
FOREIGN KEY (idcaso)
REFERENCES sgc_casos (idcaso)
ON DELETE CASCADE
ON UPDATE CASCADE
NOT DEFERRABLE;
